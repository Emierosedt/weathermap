package com.exam.em.weathermap.utils;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Created by Em on 2/20/2018.
 */

/**
 * Will generate API for Glide that allows access in RequestBuilder, RequestOptions and other APIs.
 * <p>
 * The API is generated in the same package as the AppGlideModule implementation
 * provided by the application and is named GlideApp by default.
 * <p>
 * Applications can use the API by starting all loads with GlideApp.with() instead of Glide.with():
 */
@GlideModule
public class WeatherAppGlideModule extends AppGlideModule {
}