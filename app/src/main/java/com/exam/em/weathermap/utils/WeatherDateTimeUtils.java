package com.exam.em.weathermap.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Em on 2/22/2018.
 */

public class WeatherDateTimeUtils {
    public static String getFormattedDate(long date) {
        Date c = new Date(date * 1000L); //converts the UNIX timestamp response
        SimpleDateFormat df = new SimpleDateFormat("EEE, MMM dd");
        df.setTimeZone(TimeZone.getTimeZone("UTC"));// according to the documentation, the TimeZone is UTC
        return df.format(c);
    }

    public static String getFormattedTime(long date) {
        Date c = new Date(date * 1000L); //converts the UNIX timestamp response
        SimpleDateFormat tf = new SimpleDateFormat("HH:mm z");
        tf.setTimeZone(TimeZone.getTimeZone("UTC"));// according to the documentation, the TimeZone is UTC
        return tf.format(c);
    }
}
