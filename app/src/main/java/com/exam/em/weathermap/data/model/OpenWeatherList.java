package com.exam.em.weathermap.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Em on 2/20/2018.
 */

public class OpenWeatherList implements Parcelable {
    @SerializedName("weather")
    List<Weather> weatherList;
    @SerializedName("main")
    Main mainWeather;
    @SerializedName("wind")
    Wind wind;
    @SerializedName("clouds")
    Clouds clouds;
    @SerializedName("sys")
    Sys sysDetails;
    String id;
    String name;
    @SerializedName("dt")
    long timestamp;


    protected OpenWeatherList(Parcel in) {
        id = in.readString();
        name = in.readString();
        timestamp = in.readLong();
        weatherList=   (List<Weather>) in.readValue(Weather.class.getClassLoader());
        mainWeather = in.readParcelable(Main.class.getClassLoader());
        wind = in.readParcelable(Wind.class.getClassLoader());
        clouds = in.readParcelable(Clouds.class.getClassLoader());
        sysDetails = in.readParcelable(Sys.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeLong(timestamp);
        dest.writeValue(weatherList);
//        dest.writeParcelableArray(new Parcelable[]{(Parcelable) weatherList}, flags);
        dest.writeParcelable( mainWeather, flags);
        dest.writeParcelable(wind, flags);
        dest.writeParcelable( clouds, flags);
        dest.writeParcelable( sysDetails, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<OpenWeatherList> CREATOR = new Creator<OpenWeatherList>() {
        @Override
        public OpenWeatherList createFromParcel(Parcel in) {
            return new OpenWeatherList(in);
        }

        @Override
        public OpenWeatherList[] newArray(int size) {
            return new OpenWeatherList[size];
        }
    };

    public List<Weather> getWeatherList() {
        return weatherList;
    }

    public void setWeatherList(List<Weather> weatherList) {
        this.weatherList = weatherList;
    }

    public Main getMainWeather() {
        return mainWeather;
    }

    public void setMainWeather(Main mainWeather) {
        this.mainWeather = mainWeather;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public Clouds getClouds() {
        return clouds;
    }

    public void setClouds(Clouds clouds) {
        this.clouds = clouds;
    }

    public Sys getSysDetails() {
        return sysDetails;
    }

    public void setSysDetails(Sys sysDetails) {
        this.sysDetails = sysDetails;
    }


    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public static class Main implements Parcelable{
        String temp;
        String humidity;
        String pressure;
        @SerializedName("temp_min")
        String tempMin;
        @SerializedName("temp_max")
        String tempMax;

        protected Main(Parcel in) {
            temp = in.readString();
            humidity = in.readString();
            pressure = in.readString();
            tempMin = in.readString();
            tempMax = in.readString();
        }

        public static  final Creator<Main> CREATOR = new Creator<Main>() {
            @Override
            public Main createFromParcel(Parcel in) {
                return new Main(in);
            }

            @Override
            public Main[] newArray(int size) {
                return new Main[size];
            }
        };

        public String getTemp() {
            return temp;
        }

        public void setTemp(String temp) {
            this.temp = temp;
        }

        public String getHumidity() {
            return humidity;
        }

        public void setHumidity(String humidity) {
            this.humidity = humidity;
        }

        public String getTempMin() {
            return tempMin;
        }

        public void setTempMin(String tempMin) {
            this.tempMin = tempMin;
        }

        public String getTempMax() {
            return tempMax;
        }

        public void setTempMax(String tempMax) {
            this.tempMax = tempMax;
        }

        public String getPressure() {
            return pressure;
        }

        public void setPressure(String pressure) {
            this.pressure = pressure;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(temp);
            dest.writeString(humidity);
            dest.writeString(pressure);
            dest.writeString(tempMin);
            dest.writeString(tempMax);
        }
    }

    public static class Wind implements Parcelable{
        String speed;

        protected Wind(Parcel in) {
            speed = in.readString();
        }

        public static final Creator<Wind> CREATOR = new Creator<Wind>() {
            @Override
            public Wind createFromParcel(Parcel in) {
                return new Wind(in);
            }

            @Override
            public Wind[] newArray(int size) {
                return new Wind[size];
            }
        };

        public String getSpeed() {
            return speed;
        }

        public void setSpeed(String speed) {
            this.speed = speed;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(speed);
        }
    }

    public  static  class Clouds implements Parcelable{
        String all;

        protected Clouds(Parcel in) {
            all = in.readString();
        }

        public static final Creator<Clouds> CREATOR = new Creator<Clouds>() {
            @Override
            public Clouds createFromParcel(Parcel in) {
                return new Clouds(in);
            }

            @Override
            public Clouds[] newArray(int size) {
                return new Clouds[size];
            }
        };

        public String getAll() {
            return all;
        }

        public void setAll(String all) {
            this.all = all;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(all);
        }
    }

    public static class Sys implements Parcelable {
        String country;
        String sunrise;
        String sunset;

        protected Sys(Parcel in) {
            country = in.readString();
            sunrise = in.readString();
            sunset = in.readString();
        }

        public static final Creator<Sys> CREATOR = new Creator<Sys>() {
            @Override
            public Sys createFromParcel(Parcel in) {
                return new Sys(in);
            }

            @Override
            public Sys[] newArray(int size) {
                return new Sys[size];
            }
        };

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getSunrise() {
            return sunrise;
        }

        public void setSunrise(String sunrise) {
            this.sunrise = sunrise;
        }

        public String getSunset() {
            return sunset;
        }

        public void setSunset(String sunset) {
            this.sunset = sunset;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(country);
            dest.writeString(sunrise);
            dest.writeString(sunset);
        }
    }


}
