package com.exam.em.weathermap.data;

/**
 * Created by Em on 2/20/2018.
 */

public class Constants {

    public static final String BASE_URL = "http://api.openweathermap.org/";
    public static final String CITY_IDS = "5056033,3067696,3837675";//London, Prague and San Francisco
    public static final String API_KEY = "194b5bde336f20780ab733d698a42e56";
    public static final String ICON_BASE_URL = "http://openweathermap.org/img/w/";
    public static final String UNIT = "metric";
}
