package com.exam.em.weathermap.presenters;

import android.content.DialogInterface;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;

import com.exam.em.weathermap.R;
import com.exam.em.weathermap.data.Constants;
import com.exam.em.weathermap.data.model.OpenWeatherList;
import com.exam.em.weathermap.data.model.OpenWeatherResponse;
import com.exam.em.weathermap.service.WeatherApiService;
import com.exam.em.weathermap.utils.ProgressDialogUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Em on 2/20/2018.
 */

public class WeatherPresenter {
    private static final String WEATHER_SCHED_TAG = "weather_tag";
    private static final String SOMETHING_WENT_WRONG = "Something went wrong.";

    private FragmentActivity mContext;
    private WeatherApiService mWeatherService;
    WeatherPresenterListener mWeatherListener;
    WeatherItemPresenterListener mWeatherItemListener;

    public WeatherPresenter(FragmentActivity context) {
        this.mContext = context;
        mWeatherService = new WeatherApiService();
    }

    public interface WeatherPresenterListener {
        void onWeatherApiResponse(OpenWeatherResponse response);
    }

    public interface WeatherItemPresenterListener {
        void onWeatherItemApiResponse(OpenWeatherList response);
    }

    public void getCityWeatherListDetails(WeatherPresenterListener listener) {
        showLoadingDialog(WEATHER_SCHED_TAG);
        this.mWeatherListener = listener;
        mWeatherService
                .getAPI()
                .getWeather(Constants.CITY_IDS, Constants.UNIT, Constants.API_KEY).enqueue(new Callback<OpenWeatherResponse>() {
            @Override
            public void onResponse(Call<OpenWeatherResponse> call, Response<OpenWeatherResponse> response) {
                hideLoadingDialog(WEATHER_SCHED_TAG);
                if (response.isSuccessful())
                    mWeatherListener.onWeatherApiResponse(response.body());
                else showDialog(SOMETHING_WENT_WRONG,null);
            }

            @Override
            public void onFailure(Call<OpenWeatherResponse> call, Throwable t) {
                t.printStackTrace();
                hideLoadingDialog(WEATHER_SCHED_TAG);
                showDialog(SOMETHING_WENT_WRONG,null);
            }
        });
    }

    public void getWeatherItemDetails(WeatherItemPresenterListener listener, String id) {
        showLoadingDialog(WEATHER_SCHED_TAG);
        this.mWeatherItemListener = listener;
        mWeatherService
                .getAPI()
                .getWeatherItemDetail(id, Constants.UNIT,Constants.API_KEY).enqueue(new Callback<OpenWeatherList>() {
            @Override
            public void onResponse(Call<OpenWeatherList> call, Response<OpenWeatherList> response) {
                hideLoadingDialog(WEATHER_SCHED_TAG);
                if (response.isSuccessful()) {
                    mWeatherItemListener.onWeatherItemApiResponse(response.body());
                }else {
                    showDialog(SOMETHING_WENT_WRONG,null);
                }
            }

            @Override
            public void onFailure(Call<OpenWeatherList> call, Throwable t) {
                t.printStackTrace();
                showDialog(SOMETHING_WENT_WRONG,null);
                hideLoadingDialog(WEATHER_SCHED_TAG);
            }
        });
    }

    public void showLoadingDialog(String tag) {
        ProgressDialogUtil fragment = (ProgressDialogUtil) mContext.getSupportFragmentManager().findFragmentByTag(tag);
        if (fragment == null) {
            fragment = new ProgressDialogUtil();
            fragment.setCancelable(false);
            mContext.getSupportFragmentManager().beginTransaction()
                    .add(fragment, tag)
                    .commitAllowingStateLoss();
        }
    }

    public void hideLoadingDialog(String tag) {
        ProgressDialogUtil fragment = (ProgressDialogUtil) mContext.getSupportFragmentManager().findFragmentByTag(tag);
        if (fragment != null) {
            // fragment.dismissAllowingStateLoss();
            mContext.getSupportFragmentManager().beginTransaction().remove(fragment).commitAllowingStateLoss();
        }
    }
    public void showDialog(String message, DialogInterface.OnClickListener listener) {

        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Dialog);
        } else {
            builder = new AlertDialog.Builder(mContext);
        }
        builder.setTitle(mContext.getResources().getString(R.string.app_name))
                .setMessage(message)
                .setPositiveButton("OK", listener)

                .show();
    }
}
