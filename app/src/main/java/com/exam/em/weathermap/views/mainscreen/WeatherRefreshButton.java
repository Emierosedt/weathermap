package com.exam.em.weathermap.views.mainscreen;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.exam.em.weathermap.R;
import com.exam.em.weathermap.views.WeatherItemListActivity;

/**
 * Created by Em on 2/20/2018.
 */

public class WeatherRefreshButton extends Fragment {

    public WeatherRefreshButton() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.item_weather_button, container, false);
        view.findViewById(R.id.btn_refresh).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((WeatherItemListActivity) getActivity()).reOpenWeatherList();
            }
        });
        return view;
    }
}
