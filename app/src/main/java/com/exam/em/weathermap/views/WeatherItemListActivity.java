package com.exam.em.weathermap.views;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.exam.em.weathermap.R;
import com.exam.em.weathermap.data.model.OpenWeatherList;
import com.exam.em.weathermap.views.details.WeatherItemDetailActivity;
import com.exam.em.weathermap.views.details.WeatherItemDetailFragment;
import com.exam.em.weathermap.views.mainscreen.WeatherListFragment;
import com.exam.em.weathermap.views.mainscreen.WeatherRefreshButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import java.util.ArrayList;

/**
 * An activity representing a list of Items. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link WeatherItemDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class WeatherItemListActivity extends AppCompatActivity {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());


        if (findViewById(R.id.item_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }


        /**
         *
         * Set up fragments for weather list
         * 2 fragments according to the requirement
         */
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.item_weather_button, new WeatherRefreshButton());
        transaction.add(R.id.item_weather_list, WeatherListFragment.newInstance());
        transaction.commit();

    }


    public void loadWeatherItemDetails(OpenWeatherList openWeatherresponse) {

        if (mTwoPane) {
            Bundle arguments = new Bundle();
            arguments.putParcelable(WeatherItemDetailFragment.ARG_ITEM_ID, openWeatherresponse);
            WeatherItemDetailFragment fragment = new WeatherItemDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.item_detail_container, fragment)
                    .commit();
        } else {
            Intent intent = new Intent(WeatherItemListActivity.this, WeatherItemDetailActivity.class);
            intent.putExtra(WeatherItemDetailFragment.ARG_ITEM_ID, openWeatherresponse);
            startActivity(intent);
        }

    }


    /**
     * Will re load @Link {@link WeatherListFragment}
     */
    public void reOpenWeatherList() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.item_weather_list, WeatherListFragment.newInstance());
        transaction.commit();
    }

}
