package com.exam.em.weathermap.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.exam.em.weathermap.R;
import com.exam.em.weathermap.data.Constants;
import com.exam.em.weathermap.data.model.OpenWeatherList;
import com.exam.em.weathermap.data.model.OpenWeatherResponse;
import com.exam.em.weathermap.utils.GlideApp;
import com.exam.em.weathermap.utils.WeatherDateTimeUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Em on 2/20/2018.
 */

public class WeatherItemAdapter extends RecyclerView.Adapter<WeatherItemAdapter.MyViewHolder> {
    private final OnWeatherItemClickListener mListener;
    OpenWeatherResponse mCityWeather;
    Context mContext;
    List<OpenWeatherList> mCityWeatherList;

    public interface OnWeatherItemClickListener {
        void onItemClick(int position);
    }

    public WeatherItemAdapter(OpenWeatherResponse mCityWeather, Context mContext, OnWeatherItemClickListener listener) {
        this.mCityWeather = mCityWeather;
        this.mContext = mContext;
        this.mListener = listener;
        mCityWeatherList = mCityWeather.getOpenWeatherList();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.weather_city_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.city.setText(mCityWeatherList.get(position).getName());
        holder.date.setText(WeatherDateTimeUtils.getFormattedDate(mCityWeatherList.get(position).getTimestamp()));
        holder.time.setText(WeatherDateTimeUtils.getFormattedTime(mCityWeatherList.get(position).getTimestamp()));
        holder.temp.setText(mCityWeatherList.get(position).getMainWeather().getTemp() + "\u2103");
        holder.maxTemp.setText("Max: " + mCityWeatherList.get(position).getMainWeather().getTempMax() + "\u00b0");
        holder.minTemp.setText("Min: " + mCityWeatherList.get(position).getMainWeather().getTempMin() + "\u00b0");
        holder.weatherDesc.setText(mCityWeatherList.get(position).getWeatherList().get(0).getDescription());
        GlideApp.with(mContext)
                .load(Constants.ICON_BASE_URL.concat(mCityWeatherList.get(position).getWeatherList().get(0).getIcon()).concat(".png"))
                .into(holder.iconLayout);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mCityWeatherList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_city)
        TextView city;
        @BindView(R.id.tv_date)
        TextView date;
        @BindView(R.id.tv_time)
        TextView time;
        @BindView(R.id.tv_temp)
        TextView temp;
        @BindView(R.id.tv_max_temp)
        TextView maxTemp;
        @BindView(R.id.tv_min_temp)
        TextView minTemp;
        @BindView(R.id.tv_weather_desc)
        TextView weatherDesc;
        @BindView(R.id.layout_icon)
        ImageView iconLayout;
        View itemView;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;

            ButterKnife.bind(this, itemView);
        }
    }
}
