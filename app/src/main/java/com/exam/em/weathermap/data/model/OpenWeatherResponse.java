package com.exam.em.weathermap.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Em on 2/20/2018.
 */

public class OpenWeatherResponse {
    @SerializedName("list")
    List<OpenWeatherList> openWeatherList;
    public List<OpenWeatherList> getOpenWeatherList() {
        return openWeatherList;
    }
}
