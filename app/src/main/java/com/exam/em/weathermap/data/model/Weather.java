package com.exam.em.weathermap.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Em on 2/20/2018.
 */

public class Weather implements Parcelable {
    String id;
    String main;
    String description;
    String icon;

    protected Weather(Parcel in) {
        id = in.readString();
        main = in.readString();
        description = in.readString();
        icon = in.readString();
    }

    public static final Creator<Weather> CREATOR = new Creator<Weather>() {
        @Override
        public Weather createFromParcel(Parcel in) {
            return new Weather(in);
        }

        @Override
        public Weather[] newArray(int size) {
            return new Weather[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(main);
        dest.writeString(description);
        dest.writeString(icon);
    }
}
