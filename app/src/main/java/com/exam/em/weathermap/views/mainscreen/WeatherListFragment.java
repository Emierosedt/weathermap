package com.exam.em.weathermap.views.mainscreen;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.exam.em.weathermap.views.WeatherItemListActivity;
import com.exam.em.weathermap.R;
import com.exam.em.weathermap.adapters.WeatherItemAdapter;
import com.exam.em.weathermap.data.model.OpenWeatherResponse;
import com.exam.em.weathermap.presenters.WeatherPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Em on 2/20/2018.
 */

public class WeatherListFragment extends Fragment implements WeatherPresenter.WeatherPresenterListener, WeatherItemAdapter.OnWeatherItemClickListener {
    @BindView(R.id.item_list)
    RecyclerView mWeatherList;
    private Unbinder unbinder;
    RecyclerView.Adapter mAdapter;
    private OpenWeatherResponse mOpenWeatherresponse;

    public static WeatherListFragment newInstance() {
        WeatherListFragment f = new WeatherListFragment();
        Bundle args = new Bundle();
        f.setArguments(args);

        return f;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.item_weather_list, container, false);
        unbinder = ButterKnife.bind(this, view);
        fetchWeatherList();
        return view;
    }


    private void fetchWeatherList() {
        new WeatherPresenter(getActivity()).getCityWeatherListDetails(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }


    @Override
    public void onWeatherApiResponse(OpenWeatherResponse response) {
        mOpenWeatherresponse = response;
        mAdapter = new WeatherItemAdapter(response, getActivity(), this);
        mWeatherList.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(int position) {
        ((WeatherItemListActivity) getActivity()).loadWeatherItemDetails(mOpenWeatherresponse.getOpenWeatherList().get(position));
    }

}
