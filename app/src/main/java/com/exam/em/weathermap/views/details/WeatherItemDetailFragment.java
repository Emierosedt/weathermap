package com.exam.em.weathermap.views.details;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.exam.em.weathermap.R;
import com.exam.em.weathermap.data.Constants;
import com.exam.em.weathermap.data.model.OpenWeatherList;
import com.exam.em.weathermap.presenters.WeatherPresenter;
import com.exam.em.weathermap.utils.GlideApp;
import com.exam.em.weathermap.utils.WeatherDateTimeUtils;
import com.exam.em.weathermap.views.WeatherItemListActivity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a {@link WeatherItemListActivity}
 * in two-pane mode (on tablets) or a {@link WeatherItemDetailActivity}
 * on handsets.
 */
public class WeatherItemDetailFragment extends Fragment implements WeatherPresenter.WeatherItemPresenterListener {
    @BindView(R.id.tv_city)
    TextView city;
    @BindView(R.id.tv_date)
    TextView date;
    @BindView(R.id.tv_time)
    TextView time;
    @BindView(R.id.tv_temp)
    TextView temp;
    @BindView(R.id.tv_max_temp)
    TextView maxTemp;
    @BindView(R.id.tv_min_temp)
    TextView minTemp;
    @BindView(R.id.tv_weather_desc)
    TextView weatherDesc;
    @BindView(R.id.tv_sunrise)
    TextView sunrise;
    @BindView(R.id.tv_sunset)
    TextView sunset;
    @BindView(R.id.tv_wind)
    TextView wind;
    @BindView(R.id.tv_rain)
    TextView rain;
    @BindView(R.id.tv_clouds)
    TextView clouds;
    @BindView(R.id.tv_humidity)
    TextView humidity;
    @BindView(R.id.tv_pressure)
    TextView pressure;
    @BindView(R.id.layout_icon)
    ImageView iconLayout;

    Unbinder unbinder;
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";
    public static final String ARG_WEATHER_LIST_ID = "weather_list";

    /**
     * Content for the fragment. Selected city.
     */
    private OpenWeatherList mWeatherData;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public WeatherItemDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            mWeatherData = getArguments().getParcelable(ARG_ITEM_ID);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.weather_item_details, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        processWeatherData();
    }

    private void processWeatherData() {
        city.setText(mWeatherData.getName());
        date.setText(WeatherDateTimeUtils.getFormattedDate(mWeatherData.getTimestamp()));
        time.setText(WeatherDateTimeUtils.getFormattedTime(mWeatherData.getTimestamp()));
        temp.setText(mWeatherData.getMainWeather().getTemp() + "\u2103");
        maxTemp.setText("Max: " + mWeatherData.getMainWeather().getTempMax() + "\u00b0");
        minTemp.setText("Min: " + mWeatherData.getMainWeather().getTempMin() + "\u00b0");
        weatherDesc.setText(mWeatherData.getWeatherList().get(0).getDescription());
        wind.setText(mWeatherData.getWind().getSpeed() + "m/s");
        rain.setText("");
        clouds.setText(mWeatherData.getClouds().getAll() + "%");
        humidity.setText(mWeatherData.getMainWeather().getHumidity() + "%");
        pressure.setText(mWeatherData.getMainWeather().getHumidity() + "hPa");
        sunrise.setText("Sunrise: +" + WeatherDateTimeUtils.getFormattedTime(Long.valueOf(mWeatherData.getSysDetails().getSunrise())));
        sunset.setText("Sunset: +" + WeatherDateTimeUtils.getFormattedTime(Long.valueOf(mWeatherData.getSysDetails().getSunset())));
        GlideApp.with(getActivity())
                .load(Constants.ICON_BASE_URL.concat(mWeatherData.getWeatherList().get(0).getIcon()).concat(".png"))
                .into(iconLayout);

    }

    @OnClick(R.id.btn_refresh)
    public void onRefreshData() {
        new WeatherPresenter(getActivity()).getWeatherItemDetails(this, mWeatherData.getId());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void onWeatherItemApiResponse(OpenWeatherList response) {
        mWeatherData = response;
        processWeatherData();
    }
}
