package com.exam.em.weathermap.service;

import com.exam.em.weathermap.data.Constants;
import com.exam.em.weathermap.data.model.OpenWeatherList;
import com.exam.em.weathermap.data.model.OpenWeatherResponse;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Em on 2/20/2018.
 */

public class WeatherApiService {

    public interface WeatherApi {
        @GET("/data/2.5/group")
        Call<OpenWeatherResponse> getWeather(
                @Query("id") String cityIds,
                @Query("units") String unit,
                @Query("APPID") String key
        );

        @GET("/data/2.5/weather")
        Call<OpenWeatherList> getWeatherItemDetail(
                @Query("id") String cityID,
                @Query("units") String unit,
                @Query("APPID") String key
        );

    }

    public WeatherApi getAPI() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl(Constants.BASE_URL)
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(WeatherApi.class);
    }
}
